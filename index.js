function printCurrentTime() {
  const now = new Date();
  const hours = now.getHours();
  const minutes = now.getMinutes();
  const seconds = now.getSeconds();

  console.log('Current time:', hours, ':', minutes, ':', seconds);
}

printCurrentTime();
